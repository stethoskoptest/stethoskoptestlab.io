## Platzhirsch-Alternativen?

In diesem Blog testen wir Stethoskope anderer Hersteller im direkten Vergleich gegen Modelle von Littmann&reg;.
Haptik, Qualität, Akustik und Preis-Leistungs-Verhältnis sind dabei unsere Prüfkriterien. Scrollt euch durch die Posts oder springt oben rechts im Menu direkt zu einem Modell.
