---
title: About 
subtitle: #Why you'd want to hang out with me
comments: false
---
# Idee
Jeder, der einen medizinischen Beruf ausübt oder anstrebt, stand schon einmal vor der Frage: "Welches Stethoskop brauche ich?".
Wenn man andere nach Ihrer Kaufempfehlung fragt, wird dabei nicht selten Littmann&reg; genannt. Fragt man dann nach dem warum, wird oft 
angegeben "das haben doch alle".
Dass Littmann&reg; qualitativ hochwertige Stethoskope anbietet, steht außer Frage. Was jedoch zur Frage steht, sind alternativen.
Wir haben uns auf die Suche nach anderen Produkten gemacht und testen diese im klinischen Alltag gegen Littmanns&reg; Modelle Classic III&trade; 
und Cardiology III&trade; als Referenzen
zweier unterschiedlicher Preisklassen.
Unsere Testkriterien sind dabei:

- Verarbeitungsqualität
- Haptik/ Optik
- Akustik
- Preis-Leistungs-Verhältnis

Nach und nach werden wir auf diesem Blog unsere Ergebnisse zusammentragen

Wir sind Ärzte, Studenten, Notfallsanitäter. Wir erhalten diese Blog als Hobby und bekommen keine Zuwendungen der Hersteller. 
Wir halten die Tests so objektiv wie möglich, die akustische Wahrnehmung ist und bleibt aber sehr subjektiv.

# Kontakt
*  Mail:     scopetest<i></i>@mailfence.com
*  XMPP:        cpt<i></i>@<i></i>anonym.im
