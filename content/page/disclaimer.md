---
title: Disclaimer 
subtitle: #Why you'd want to hang out with me
comments: false
---
# Disclaimer
Stethoskope von Littmann (R) sind der Goldstandard der Auskultation. Die Qualität und akustische Performance stehen hier völlig außer Frage. Wir stellen hier lediglich alternative Produkte im direkten Vergleich vor und beurteilen, ob diese dem Vergleich standhalten.

Unsere Tests und Empfehlungen sind **rein subjektiv** und nicht objektiv verifiziert. Wir erhalten keinerlei finanzielle Zuwendungen der Hersteller. Einige der Testmodelle wurden uns permanent überlassen.