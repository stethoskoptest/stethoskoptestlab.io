---
title: ERKA Finesse / Finesse²
subtitle: 
date: 2020-01-16
tags: []
---

*  Preis: ca. 45€
*  Optik ★★☆☆☆
*  Haptik ★★★☆☆
*  Qualitätseindruck ★★☆☆☆
*  Akustik ★★☆☆☆
*  Lieferumfang/Ersatzteile ★★★☆☆
*  GESAMT ★★☆☆☆
*  **Preis-Leistung neben Littmann Classic III: unterlegen**
*  Link: https://www.erka.org/de/stethoskope/finesse/23
*  Link: https://www.erka.org/de/stethoskope/finesse%C2%B2/24


Für Studenten: ★★☆☆☆
Für Anästhesisten: ★☆☆☆☆
Für Kardiologen: ☆☆☆☆☆

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="/img/erka1.png" caption="Sphere" >}}
  {{< figure thumb="-thumb" link="/img/erka2.png" caption="Sphere" >}}
  {{< figure thumb="-thumb" link="/img/erka4.png" caption="Sphere" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Hintergrund
ERKA = Er + Ka = R + K = Richard Kallmeyer. Dieser gründete seine Fabrik für medizinische Diagnostika bereits 1889 in Berlin. Mittlerweile hat ERKA eine sehr breite Stethoskoppalette. Grundsätzlich findet sich zu nahezu jedem Littmann-Produkt eine ERKA-Alternative.
Am 23.12. (also einen Tag vor Heiligabend!) gegen 12:00 Uhr haben wir poer E-Mail Kontakt zu ERKA aufgenommen; 4 Stunden später haben wir bereits eine Antwort mit der Zusage, man werde uns im Januar Testgeräte zuschicken. Wir haben sowohl ein Finesse mit Membran und Trichter, als auch ein Finesse² mit zwei Membranen zum Test erhalten. Der Kontakt zu ERKA war sehr angenehem und stets sehr entgegenkommend.


## Optik
Die Finesses sind vom Aufbau dem Classic III schon ähnlinch. Unterschiede finden sich in der ERKA-typischen Ohrbügelkröpfung (Bügel deutlich dicker als bei Littmann) und dem Y-Schlauch (der hier ein Doppellumenschlauch ist, was man in dieser Preisklasse sonst nirgends findet!!). Die Membranen sind etwas kleiner im Durchmesser.

## Haptik 
Solide! Dicke Ohrbügel (dicker als beim Classic III), wie beim Precise ein Doppellumenschlauch, feste Membran (ohne Dual-Frequency-Technik), schöne weiche Oliven. Die Ohrbügel, die auch hier einstellbar sind, sind wieder weniger stramm im Ohr, als die des Littmanns, das kostet Punkte bei der Isolation (s.u.) und fühlte sich in unseren Gehörgängen einfach wenig brauchbar an. Das gilt übrigens für alle getesteten ERKAs. Der Schlauch fasst sich gut an, ist nicht zu fest oder zu labbrig, also genau richtig, das Gewicht ist dem des Classic sehr ähnlich und für Freunde der Hals-Boa ist das Gewicht sehr gut austariert.


## Verarbeitung / Qualität
So sehr wir das ERKA Precise gelobt haben, mussten wir bei beiden Finesse sehr schnell feststellen, dass der Antikältering eine echte Fehlkonstruktion ist: in beiden Modellen verabschiedete sich dieser in der Kitteltasche bereits nach wenigen Minuten mehrmals. Bei einem der beiden Modelle riss der Ring dann auch noch beim **ersten** Reponierversuch. Auch in dieser Preisklasse darf sowas nicht sein!
Der Rest wirkt eigentlich ähnlich gut verarbbeitet wie das Precise.

## Akustik
Auch bei Nachjustierung der Ohrbügel isoliert das Finesse deutlich schlechter als das Classic III, das bereitete bei unserem Test sowohl in der Anästhesie, als auch in der Notaufnahme massive Probleme durch Nebengeräusche. Die Nutzbarkeit für den Rettungsdienst ist damit gänzlich hinfällig. Neben dem Classic III ist der Ton dann auch noch deutlich leiser und muffig-dumpfer. Diese Kombination hat uns im Test stark misfallen. Für eine Herzauskultation muss die Membran unverhältnismäßig stark auf den Thorax pressen, was bei den Patienten bereits unangenehm ist. Atemgeräusche lassen sich nur bei geräuscharmer Umgebeung sicher diagnostizier; selbst die Tubuslage nach intubation war mit dem Finesse nicht *sicher* bestätigen. Für einen Hausarzt mit ruhiger Praxis mag das ausreichen, für alle klinische Szenarien finden wir beide Finesse-Varianten (leider) wenig brauchbar. 

## Ersatzteile/Lieferumfang
*  Stethoskop
*  Tasche
*  1 Paar Oliven gleicher Größe, höherer Härtegrad

Bei all der Kritik wenigstens wie bei allen ERKAs eine Tasche. Das hellt den Gesamteindruck etwas auf. Aber nur etwas. Ersatzteile wie Membran oder Oliven  können direkt über ERKA bezogen werden. Die Garantie kann per Registrierung auf 5 Jahre verlängert werden.

## Preis-Leistung
Der Kälteschutzring und die akustische Unterlegenheit ggü. Littmann sind echte Negativbrocken. Auch wenn das ERKA Finesse(²) nur die Hälfte des Classic III kostet, können wir keine Empfehlung aussprechen. Klar, ein günstiger Preis, aber selbst dafür stimmt die Performance einfach nicht. Vielleicht justiert ERKA noch nach, an sich bringt das Finesse nämlich tolle Specs mit (Doppellumenschlach, Y-Stück, dicke Ohrbügel).

## Gesamteindruck
Für nicht mal 20€ mehr sollte man einfach direkt zum Precise aus dem selben Haus greifen, wobei auch hier die Ohrbügel zu locker sitzem, der Rest passt dort aber.