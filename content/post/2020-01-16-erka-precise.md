---
title: ERKA Precise
subtitle: 
date: 2020-01-15
tags: []
---

*  Preis: ca. 67€
*  Optik 2.5/5
*  Haptik 3.5/5
*  Qualitätseindruck 4/5
*  Akustik 5/5
*  Lieferumfang/Ersatzteile 4/5
*  GESAMT 3.5/5
*  **Preis-Leistung neben Littmann Classic III: berlegen**
*  **Preis-Leistung neben Littmann Cardiology III: weit überlegen**
*  Link: https://www.erka.org/de/stethoskope/precise/21

Für Studenten: 5/5
Für Anästhesisten: 2/5
Für Kardiologen: 2/5

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/precise1.jpg" caption="" >}}
  {{< figure link="/img/precise23.jpg" caption="" >}}
  {{< figure link="/img/precise20.jpg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Hintergrund
ERKA = Er + Ka = R + K = Richard Kallmeyer. Dieser gründete seine Fabrik für medizinische Diagnostika bereits 1889 in Berlin. Mittlerweile hat ERKA eine sehr breite Stethoskoppalette. Grundsätzlich findet sich zu nahezu jedem Littmann-Produkt eine ERKA-Alternative.
Am 23.12. (also einen Tag vor Heiligabend!) gegen 12:00 Uhr haben wir poer E-Mail Kontakt zu ERKA aufgenommen; 4 Stunden später haben wir bereits eine Antwort mit der Zusage, man werde uns im Januar Testgeräte zuschicken.


## Optik
Gerade im Vergleich zu den durchaus gut aussehenden All-Black-Littmännern wirkt das Precise etwas gewöhnungsbedüftig (wie alle ERKA-Modelle). Die Kröpfung der Ohrbügel, das sehr kurze Y-Stück, die riesige Membran und der winzige Trichter wirken irgendwie merkwürdig. Wem die Optik wichtig ist (was objektiv Blödsinn ist), dem könnte das missfallen.

## Haptik 
Solide! Dicke Ohrbügel (dicker als beim Classic III), dicker Einzellumenschlauch, feste Membran (die übrigens wie Littmann mit Dual-Frequency-Technik), schöne weiche Oliven. Letztere dichten aufgrund eines zu leicht gewählten und dazu schlecht justierbarem Anpressdruck relativ schlecht nach außen ab, dadurch fallen störende Nebengeräusche deutlich auf. Das Gewicht passt genau, auch um den Hals geschlungen geben sich Bruststück und Ohrbügel gut die Waage.

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/precise19.jpg" caption="" >}}
  {{< figure link="/img/precise17.jpg" caption="" >}}
  {{< figure link="/img/precise11.jpg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Verarbeitung / Qualität
Man muss wirklich suchen, um Schwachstellen zu finden. Bei ganz genauem Blick war bei unserem Gerät die Kröpfung der beiden Ohrbügel um etwa 1° unterschiedlich. Wessen Gehörgänge sind 100% symmetrisch? Genau! Die Littmänner haben da übriges auch eine kleine Variabilität. Runddum ist das Precise top verarbeitet. Negativ fiel uns (wie bei den Modellen Finesse übrigens auch) auf, dass der Kaälteschutzring viel zu locker an der Membran aufsitzt und dadurch schnell verloren geht.
Übrigens: ERKA setzt auf poliertes Messing. Laut eigener Aussage, seien die Poren  darin kleiner als in Edelstahl (den Littmann verwendet). Ob das stimmt lassen wir mal im Raum stehen.

## Akustik
Wir halten es kurz: Besser als das Classic III, ebenbürtig zum Cardiology III. Dabei erscheint einem unserer Tester das Precise einen Hauch dumpfer als das Cardiology. Die größere Membran ist akustisch ein echter Vorteil. Der kleinere Trichter dagegen nimmt den Schall deutlich weniger gut auf. Im Klinikalltag ist das tatsächlich ein ziemliches Problem (Wie bei den anderen ERKA-Modellen). Was nützt die beste Akustik, wenn das Husten des Patienten im NAchbarbett lauter ist, als der Auskultationston. Schade, denn der Ton an sich ist hervorragend austariert. Übrigens ist der Trichter nicht konvertierbar, also keine Kinderauskultation mit dem Precise.

## Ersatzteile/Lieferumfang
*  Stethoskop
*  Tasche
*  1 Paar Oliven gleicher Größe, höherer Härtegrad

Eine Tasche! Einfach so dazu! Für Littmänner muss man sowas gesondert kaufen, was meist mit 25€ zu Buche schlägt. Bei ERKA gibts das gratis dazu. Der Rest des Lieferumfang ist dagegen eher spartanisch. Die Ersatzteilbeschaffung ist bei ERKA direkt oder bei Retailern möglich. Eine Garantierverlängerung auf 5 Jahre ist ebenfalls durch Registrierung machbar.

## Preis-Leistung
Für nicht mal 70€ bekommt man mit dem ERKA Precise ein Qualitätsstethoskop made in Germany, dass sich akustisch nicht vor dem Cardiology III und schon gar nicht vor dem Classic III verstecken muss. Dazu gibt es noch eine Tasche und einen super Kunden-Support. Wir sind beeindruckt, was das Ding für das Geld liefert. Dabei sehen wir aber 2 fette Minuspunkte: Der fehlende Anpressdruck der Ohrbügel und den zu lockeren Kälteschutzring. Wären diese zwei Dinge nicht, würden wir volle Punktzahl geben. 

## Gesamteindruck
Ist das Precise nun eine Alternative? Das kommt darauf an, wo es eingesetzt wird. Ruhige Praxis und Stationsalltag -- go for it. Notaufnahme, OP und NAW -- eher nicht so. Uns gefällt, dass ERKA sich traut, gewohnte PFade zu verlassen und wir sehen viele sehr sehr gute Eigenschaften des Precise. Bei den beiden genannten Negativpunkten muss man allerdings überlegen, was man braucht und will. Die rein akustik ist jedenfalls für das Geld spitze.