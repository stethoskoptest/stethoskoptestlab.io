---
title: Riester Cardiophon 2.0
subtitle: 
date: 2020-01-06
tags: []
---


*  Preis: ca. 80€
*  Optik: 5/5 
*  Haptik: 4.5/5
*  Qualitätseindruck: 5/5 
*  Akustik: 5/5 
*  Lieferumfang/Ersatzteile: 4/5
*  GESAMT: 5/5
*  **Preis-Leistung neben Littmann Cardiology III überlegen**
*  Link: https://www.riester.de/de/produktdetails/d/Produkte//cardiophon-20-stethoskop/

Für Studenten: ★★★★★|  Für Anästhesisten: ★★★★☆  | Für Kardiologen: ★★★★★

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/cardiophon3.jpg" caption="" >}}
  {{< figure link="/img/cardiophon5.jpg" caption="" >}}
  {{< figure link="/img/cardiophon10.jpg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Hintergrund
Eine Anfrage-Email, ein nettes kurzes Telefonat und 10 Tage später haben wir zwei Modelle von Riester zum Test auf dem Tisch liegen. Die Firma gibt es bereits seit 1948 und wurde von Rudolf Riester in Jungingen in Baden-Württemberg gegründet. Neben Stethoskopen führt Riester auch Otoskope, Bludruckmessgeräte und weitere Diagnostika.
Das Cardiophon 2.0 ist ein - wie der Name verrät - ein Kardiologiestethoskop mit Doppelkopf mit einer mechanischen Membran ohne Dual-Frequency, je eine Seite Erwachsene, eine Seite Kinder.


## Optik
Das Modell, das uns zur Verfügung gestellt wurde, zeigt sich in Burgund mit unlackierten Edelstahlteilen. Auffällig schon beim Anschauen ist der dicke Schlauch. Unserer Meinung nach darf ein Kardio-Stethoskop das so machen. Der Rest sieht gut aus. Auch hier gefällt uns die Optik des Bruststücks und der schwarzen Membran mit Riester-Emblem.

## Haptik 
Etwas steifer und schwerer als das Cardiology III, aber wie gesagt: der Name Cardiophon ist hier eben Programm. Für die Kitteltasche ist das Gerät zu groß und sperrig, um den Hals geschlungen kann man es eine Zeit lang tragen, aber wohl kaum einen ganzen Dienst hindurch. Zwischndrin sollte man es einfach einmal ablegen oder direkt zu einem Holster greifen. Das Littmann ist da schon leichter und kann durchaus stundenweise als Halkette getragen werden. Wie schon beim Duplex 2.0 hat auch das Cardiophon die rotierenden Oliven. Ob Zufall oder Konzept: es stört nicht, ist nur ungewohnt. Der Anpressdruck der Ohrbügel ist genau richtig, die Isolation nach außen ist damit sehr gut gelungen.

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/cardiophon7.jpg" caption="Riester Cardiophon 2.0" >}}
  {{< figure link="/img/cardiophon8.jpg" caption="Riester Cardiophon 2.0" >}}
  {{< figure link="/img/cardiophon6.jpg" caption="Riester Cardiophon 2.0" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Verarbeitung / Qualität
Nichts zu beanstanden oder gesondert zu betonen. Die Qualität stimmt.

## Akustik
Auch beim Cardiophon hat uns Riester wirklich überzeugt. Der Ton des Cardiophons erscheint uns einen Hauch klarer als beim Littmann, wobei wir auch hier nicht sagen können, inwieweit die Dual-Frequency-Membran des Cardiology im Vergleich zur klassischen Membran des Riesters den Ton verändert. Die Lautstärke ist in etwa gleich, auch hier hängt es vom Anpressdruck vor allem des Littmanns ab. Alles in allem können wir sagen, dass die akustische Performance beider Geräte in einer Liga spielen. Subjektiv gefällt und der Ton des Riesters etwas besser.

## Ersatzteile/Lieferumfang
*  Stethoskop
*  je 1 Ersatzmembran
*  2 Paar Oliven gleicher Größe, gleicher Härtegrad
*  Name-Tag

Alle Bauteile des Duplex sind einzeln nachbestellbar (auch Bruststück, Kälteringe etc.).

## Preis-Leistung
Für etwa 80€ (je nach Shop) bietet das Riester Cardiophon 2.0 quasi dieselbe Performance des Littmann Cardiology III. Ja, es wirkt etwas behäbiger und verzichtet auf Dual-Frequency-Technologie, aber alles in Allem geht das für den Preis absolut in Ordnung. Uns hat das Konzept überzeugt. Für angehende oder fertige Kardiologen bietet sich hier eine echte Alternative zum schmalen Preis. Für Allgemeininternisten, ambitionierte Anästhesisten oder Notfallmediziner wird allenfalls die etwas verminderte Handlichkeit ein Problem darstellen. Aber nochmal: Ein vollwertiges Kardiologiestethoskop für nicht mal nen Hunderter, das ist gelinde gesagt der Hammer! Eines der besten von uns getesteten Stethoskope!

