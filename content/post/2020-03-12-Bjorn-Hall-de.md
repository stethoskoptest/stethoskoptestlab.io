---
title: Björn Hall
subtitle: Swedish design from Downunder
date: 2020-03-13
tags: []
---
## English version below
*  Preis: 99 $
*  Optik: 4.5/5 
*  Haptik: 4.5/5
*  Qualitätseindruck: 5/5 
*  Akustik: 5/5 
*  Lieferumfang/Ersatzteile: 5/5
*  GESAMT: 4.5/5
*  **Preis-Leistung dem Littmann Cardiology III gleichwertig**
*  Link: https://bjornhall.com/collections/stethoscopes/products/stethoscopes

Für Studenten: ★★★★★ | Für Anästhesisten: ★★★★★| Für Kardiologen: ★★★☆☆

Hinter der Firma **Björn Hall** verbirgt sich Jenny Kronqvist. Jenny ist von Hause aus Krankenschwester. Die in Australien lebende gebürtige Schwedin beitreibt einen Youtube-Kanal ([Link](https://www.youtube.com/channel/UCorbVHwvQaLByF42n1YUGaw)), auf dem Sie Stethoskope aller möglichen Hersteller gegeneinander testet. Aus europäischer Sicht ist dabei schade, dass sie fast ausschließlich Hersteller aus den vereinigten Staaten zum Review zur Verfügung hat.
Nachdem sie selber von der Omnipräsenz von Littmann-Stethoskopen "genervt" war und durch ihre Tests die Pros & Cons der von ihr getesteten Stethoskope gesehen hat, hat sie kurzerhand ihr eigenes Brand gegründet und produziert nun ihr "Björn Hall Stethoscope" ([Link](https://bjornhall.com)), wobei sie die Schwächen anderer Produkte versucht zu verbessern. Bislang ist das Björn Hall nur in Australien und den USA erhältlich, Jenny hat uns jedoch versichert, dass sie bereits an einem Vertrieb für Europa arbeitet. Ihr werdet das gute Stück also vermutlich im Laufe dieses Jahres bestellen können. Nach einem sehr netten E-Mail_Kontakt hat Jenny uns kurzerhand eines ihrer Stethoskope zum Test zugeschickt. Dieses musste sich im Test gegen das Littmann Classic III behaupten.
Beim Auspacken haben wir uns als erstes über einen kurzen handschriftlichen  Brief gefreut. Dabei müssen wir betonen, dass die gesamte Korrespondenz und das Gesamtbild von Björn Hall authentisch wirkt und einen absolut sympathischen Eindruck hinterlässt. **So viel vorneweg: We love Jenny!**

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/Bjoernhall18.jpg" caption="" >}}
  {{< figure link="/img/Bjoernhall3.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall20.jpg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Haptik / Optik
Auch das Björn Hall hat einen Doppelkopf mit Membran, wobei die Kindermembran in einen Trichter umgewandelt werden kann. Direkt beim Auspacken unseres "All matte black"-Modells fällt eine deutliche Ähnlichkeit zum Classcic III in ebenfalls "All-Black" auf. Unterschiede finden sich dann beim genauen Hinsehen jedoch einige. Vom Gewicht her geben sich beide Modelle nicht viel. Das Geschläuch des Björn Hall ist etwas steifer, ist etwa 3cm länger und hat eine mattere Oberfläche, was das Tragen um den Hals angenehmer machen soll. Uns gefällt das. Die Membran (keine Dual-Frequenzabdeckung) bzw. das Bruststücks sind etwas größer als beim Classic III, die Ohrbügel haben einen identischen Anpressdruck und damit eine sehr gute Isolation nach außen. Die Oliven sind deutlich weicher, als die des Littmanns, was in der Handhabung einen kleinen Nachteil bietet (genaueres dazu unten). Optsich etwas unschön gelöst empfinden wir die Aufschrift "Designed in Stockholm , assembled in China" auf dem Ohrbügel. Jetzt könnte man sagen, dass das einfach konsequent ehrlich ist, dennoch stört das die Ästhetik dieses rundum hübschen Stethoskops schon deutlich.

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/Bjoernhall10.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall11.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall12.jpeg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Verarbeitung / Qualität
Sowohl beim ersten Blick und Anfassen, als auch bei der Benutzung in der Klinik gibt es von der Verarbeitung herr nichts zu meckern. Nichts ist lose, nichts wirkt billig. Einzig durch den Drehmechanismus für den Membranwechsel meinen wir etwas an Lack verloren zu haben (s. Bild), aber das ist eigentlich nebensächlich und fällt nur auf, wenn man das Gerät mit Argusaugen nach Fehlern absucht. Der Eindruck des Unboxings bestätigt sich also vollends. Ein kleiner Wehmutstropfen ist die zu weiche Konsistenz der Oliven (s.u.).

## Akustik
Die akustische Performance ist sehr gut. Durch die gute Isolation ist der Ton immer laut genug. Qualitativ konnten wir zwischen Björn Hall und Classic III kaum einen Unterschied ausmachen. Wenn überhaupt, ist die Auflösung beim Björn Hall etwas klarer als beim Littmann, aber dieser Unterschied ist wirklich nur marginal. Was uns nicht gefallen hat, war in ein paar Situationen, dass eine der sehr weichen Oliven beim ansetzen ans Ohr umknickten, und wir somit auf dem Ohr taub waren. Dies ist dem besseren Komfort im Vergleich zum Littmannn geschuldet. Man muss das Björn Hall eben "bewusster" aufsetzen. Wir deklarieren daher ein klares Unentschieden in Punkto Akustik. 

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/Bjoernhall17.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall8.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall21.jpeg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Ersatzteile/Lieferumfang
*  Stethoskop
*  3 paar Ersatz-Oliven in drei versch. Größen
*  Je eine Ersatzmembran
*  1 Kälteschutzring für Trichter-Konfiguration
*  Ein Spare-Kit-Bag

Einzelteilbestellungen sind derzeit noch nicht verfügbar, laut Jenny aber in Planung.
> Das ist, als würde man 'ne teure Uhr auspacken. Find ich geil!

Das sagte einer unserer Tester beim Unboxing. Und tatsächlich ist das Björn Hall mit viel Liebe zum Detail verpackt. Kein Billo-Karton, keine Plastiktüten, Visitenkarten statt Fledder-Papier-Manuals. Das macht richtig Spaß. Sehr gut finden wir auch die Tasche für die Ersatzteile. Für die "User-Experience" beim Auspacken vergeben wir 12 von 10 Punkten!

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/Bjoernhall6.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall7.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall3.jpeg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

# Preis-Leistung
Hinsichtlich der Preisleistung geben sich das Björn Hall Stethoskop und das das Littmann Classic III nicht viel. Preis, Qualität, Performance und Haptik sind ähnlich bis gleich. Durch die Goodies wie die Ersatzteiltasche und die wirklich nicht zu verachtende Verpackung sehen wir das Björn Hall einen Schritt vorne. Unsere Sympathien für Jenny ergeben einen weiteren Schritt.

# Gesamteindruck
Wir mögen das Björn Hall. Sehr. Allerdings stellt sich bei ähnlichem Preis tatsächlich die Frage nach der Zielgruppe. Wir kommen zu dem Schluss, dass das Björn Hall das richtige Stethoskop für all diejenigen ist, die die Classic III-Performance suchen, einen Hauch Individualität im Klinik-Einheitsbrei mögen und lieber ein junges und ambitioniertes Startup anstatt einens Milliardenschweren Konzern unterstützen wollen.

Katzen würden Whiskas kaufen, wir das Björn Hall Stethoskop!

---

---
*  Price: 99 $
*  Appearance: 4.5/5 
*  Feel: 4.5/5
*  Build Quality: 5/5 
*  Acoustics: 5/5 
*  Delivery & Spare Parts: 5/5
*  TOTAL: 4.5/5
*  **Price-to-value compared to the Littmann Classic III: equal**
*  Link: https://bjornhall.com/collections/stethoscopes/products/stethoscopes

For Students: ★★★★★ | For ER/EMT/ICU ★★★★★ | For Cardiologists ★★★☆☆

After a very nice email correspondance, Jenny sent us one of her stethoscopes for our comparison with the Littmann Classic III. While unboxing, the first thing that came to view was a handwritten letter from Jenny. This, the email contact to her and the whole impression of Björn Hall are just authentic and adorable. For the prerecord: **We love Jenny!**

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/Bjoernhall18.jpg" caption="" >}}
  {{< figure link="/img/Bjoernhall3.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall20.jpg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Look and Feel
Right out of the box, the Björn Hall has a lot of specs in common with the Classic III. But while comparing, we found some differences, that might matter a lot.
The Björn Hall, like the Classic III, has a double sided chest piece with two diaphragms, one for adults and one pediatric. The pediatric side can be tranformed into a open bell. Both test candidates are coloured in "all matte black", where the Classic III is a bit more glossy than the Björn Hall. We like the mattish look and fell of the Björn Hall more, especially when wearing it around the neck. The Björn Hall's tubing is ab bit stiffer and thicker, which makes it feel a bit more durable and makes the impression of higher build quality at first glance. The tube is about 3cm longer. This is supposed to reduce the distance to the patient in order to prevent from back pain of the examiner. In use, we didn't feel that much of a difference, but for larger people it might be an advantage. The weight of both products is about the same.

The adult diaphragm of the Bjkörn Hall is ab bit bigger, the binaurals have a perfectly adjustable pressure on the ear pieces, the background noise isolation is very good, nearly the same as for the Classic III. The ear pieces are comfortable softer than the Littmann's, but this brings a disadvantage: in some cases, the ear piece kinks at the middle part, so the tone transmission was discontinued. This can be prevented by putting it carefully into the ear channel. This is not a big deal, but the contender from Littmann just doesn't have this issue. 

From our point of view, a very unpleasant detail is the inscription "Designed in Stockholm , assembled in China" on the binaurals. One could say "this is just honest" but in our opinion, this disrupts the aesthetics of the overall beautiful stehoscope.

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/Bjoernhall10.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall11.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall12.jpeg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Build quality
Even after a few weeks of usage in hospital, the overall quality is perfect. Everything stays in place and looks fine. The only worn detail is the colour layer of the turn tubing. But this is a spot of every day moving, so not a big deal.

## Acoustics
Short statement: Very very good! The volume and the tone quality are easily comparable to the Classic III. Actually, the tone was a tiny bit clearer/crisper and louder compared to the Classic III. We are very impressed! The volume together with the excellent isolation make it usable even in loud surroundings like ER, ICU or even on the streets or in an ambulance (EMTs out there: go get it!).

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/Bjoernhall17.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall8.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall21.jpeg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Unboxing/Spare parts
*  Stethoscope
*  3 pair ear pieces in different sizes
*  1 diaphragm for each size
*  1 anti-chill ring for bell usage
*  A spare-kit bag

Single part orders may be planned for the future.
> It feels like unboxing an expensive swiss watch! I f***ing love it!

That's the comment from one of our testers during the first unboxing. And indeed, the Björn Hall stethoscope is packed with lots of love for tiny details: No cheap cartonage, no plastic bags, buisness cards with information instead of standard manuals. And a bag for spare parts, that is so great! For the user experience we give 12 out of 10 points ;-)


{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/Bjoernhall6.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall7.jpeg" caption="" >}}
  {{< figure link="/img/Bjoernhall3.jpeg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

# Value for the price
Concerning what you get for the dollar, the Björn Hall stethoscope and the Littmann Classic III play in the same league. Price, build quality and look & feel are similar. But with all the tiny details in the construction, the nice package, the extra bag for spare parts and the (bit) better acoustic the Björn Hall has an advance to the Classic III.  Knowing that behind all that is a lovely Swedish nurse from Australia makes this advance even bigger.

# Total Result
We like the Björn Hall stethoscope. A lot. But at this point one has to ask for the target audience for this product since the Classic III is also a very good choice. We suggest buying the Björn Hall for all people, who search for a decent Classic-III-performance, who like the individual touch in the all-Littmann-in-the-hospital-swamp and to those, who would like to support a young and ambitious start-up instead of a multi-million corporation. 

And to all proud parents: it makes also a perfect gift for med-students, nurses and paramedics!