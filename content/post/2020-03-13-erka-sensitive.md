---
title: ERKA Sensitive
subtitle: 
date: 2020-03-13
tags: []
---

*  Preis: ca. 90€
*  Optik: 3.5/5
*  Haptik: 3.5/5
*  Qualitätseindruck: 4/5
*  Akustik: 4.5/5
*  Lieferumfang/Ersatzteile: 4/5
*  GESAMT: 4/5
*  **Preis-Leistung neben Littmann Classic III: überlegen**
*  **Preis-Leistung neben Littmann Cardiology III: gleichwertig**
*  Link: https://www.erka.org/de/stethoskope/sensitive/20

Für Studenten: ★★★☆☆ | Für Anästhesisten: ★★☆☆☆ | Für Kardiologen: ★★★☆☆

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/sensitiv7.jpg" caption="Sensitive" >}}
  {{< figure link="/img/sensitiv1.jpg" caption="Sensitive" >}}
  {{< /gallery >}}
{{< load-photoswipe >}}

## Hintergrund
ERKA = Er + Ka = R + K = Richard Kallmeyer. Dieser gründete seine Fabrik für medizinische Diagnostika bereits 1889 in Berlin. Mittlerweile hat ERKA eine sehr breite Stethoskoppalette. Grundsätzlich findet sich zu nahezu jedem Littmann-Produkt eine ERKA-Alternative.
Am 23.12. (also einen Tag vor Heiligabend!) gegen 12:00 Uhr haben wir poer E-Mail Kontakt zu ERKA aufgenommen; 4 Stunden später haben wir bereits eine Antwort mit der Zusage, man werde uns im Januar Testgeräte zuschicken. 
Das Sensitive muss sich aufgrund von Preis und Anlehnung an das Littmann Master Cardiology mit dem Cardiology III messen, während wir aufgrund des geringeren Preises das Precise gegen das Classic III haben antreten lassen.


## Optik
Das Sensitive hat das gleiche Schlauch- und Ohrbügelsystem wie das Precise von ERKA. Das Bruststück hingegen erinnert sehr stark an die Master-Serie von Littmann, vor allem an das Flagship Cardiology Master. Dabei ist die Optik durch das eher gewohnte Bild des Bruststücks nicht ganz so "eigenständig" wie beim Precise.

## Haptik 
Auch hier wieder ein solider Eindruck mit leider denselben Schwächen wie beim Precise. Dicke Ohrbügel, dicker Doppellumenschlauch, feste Membran (die übrigens wie Littmann mit Dual-Frequency-Technik), schöne weiche Oliven. Letztere dichten aufgrund eines zu leicht gewählten und dazu schlecht justierbarem Anpressdruck relativ schlecht nach außen ab, dadurch fallen störende Nebengeräusche deutlich auf. Das Gewicht passt genau, auch um den Hals geschlungen geben sich Bruststück und Ohrbügel gut die Waage, das Bruststück ist -- wie es sich für eine Kardio-Scope gehört -- relativ schwer und bleibt auch ohne Fixierung auf dem Patienten liegen. Negativ finden wir die Fixierung des Kälteschutzringes: dieser hat zur Membranseite hin etwas Spiel, wodurch er sich mitunter auf die Membran anlegt und dadurch die Schwingung und damit die Tonübertragung behindert. Sehr ärgerlich, zumal der Ton an sich sehr gut ist (s.u).

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/sensitiv2.jpg" caption="" >}}
  {{< figure link="/img/sensitiv5.jpg" caption="" >}}
  {{< figure link="/img/sensitiv10.jpg" caption="" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

## Verarbeitung / Qualität
Neben dem (wieder) zu geringem Anpressdurck der Ohrbügel und der Sache mit dem kaältschutzring finden wir nichts gravierendes hinsichtlich der Verarbeitung. Wie auch das Precise ist das Sensitive grundsätzlich anständig verabreitet.
Übrigens: ERKA setzt auf poliertes Messing. Laut eigener Aussage, seien die Poren  darin kleiner als in Edelstahl (den Littmann verwendet). Ob das stimmt lassen wir mal im Raum stehen.

## Akustik
Sofern der genannte Kälteschutzring korrekt aufgesetzt wird, ist beim Sensitive der Name Programm. Das Top-Modell von ERKA hat einen der lautesten Töne im gesamten Testfeld. Einigen könnte das schon zu laut sein, Kollegen mit nachlassender Hörleistung könnten das ls Vorteil werten. Hinsichtlich der Toncharakteristik funktioniert die Dual-Frequency-membran (die übrigens ander als bei Littmann aus der eigentlichen Membran und einer zweiten, darunter liegenden MEtallmembran in Wellenform besteht) sehr gut. Im Vergleich zum Cardiology III ist der Ton minimal dumpfer, aber nicht wesentlich schlechter. Wir sagen: Beide bewegen sich auf gleichem niveua, mit einem Vorsprung von 0,28nm für das Littmann.

## Ersatzteile/Lieferumfang
*  Stethoskop
*  Tasche
*  1 Paar Oliven gleicher Größe, höherer Härtegrad

Eine Tasche! Einfach so dazu! Für Littmänner muss man sowas gesondert kaufen, was meist mit 25€ zu Buche schlägt. Bei ERKA gibts das gratis dazu. Der Rest des Lieferumfang ist dagegen eher spartanisch. Die Ersatzteilbeschaffung ist bei ERKA direkt oder bei Retailern möglich. Eine Garantierverlängerung auf 5 Jahre ist ebenfalls durch Registrierung machbar.

## Preis-Leistung
Für um die 90€ bietet ERKA also eine Art Cardiology-Master-Replique. Das ist etwa die Hälfte dessen, was das Cardiology III kostet(e). Dafür bekommt man an sich eine gut bis gute Akustik, die eigentlich nur durch die schlechte Geräuschisolation nach außen und die GEfahr des den TOn behindernden Kälteschutzrings getrübt wird. Wären diese beiden Punkte nicht, würde wir (auch hier) eine uneingeschränkte empfehlung aussprechen. Nichtsdestotrotz ist das Sensitive von ERKA ein Qualitätsprodukt, das einiges zu bieten hat.

## Gesamteindruck
Eigentlich ist alles gesgat. Zielgruppe? Alle, die internistisch unterwegs sind und eine ruhige Arbeitsumgebung haben, das sollte passen. 